#use clutsersim for normalisation
#install.package("clusterSim")

#used for svm and cross validation 
#install.packages("caret")
#install.packages("e1071")

#useful package, not using them  yet
#install.packages("dplyr")
#install.packages('rmarkdown')
#used for correlation matrix
#http://www.r-bloggers.com/graphically-analyzing-variable-interactions-in-r/
#install.packages("PerformanceAnalytics")

library(PerformanceAnalytics)
library(clusterSim)
library(caret)
library(dplyr)
library(e1071)
?tune
#change this to your directory
setwd("F:\\ncirl\\adm\\ca\\ca1\\kaggle")

#data as is from csv file
raw_data = read.csv(file="train.csv", header = TRUE,sep = ",")
str(raw_data)

#exclude the Id column
#raw_data$Id <- NULL

#?hist #?seq 
table(raw_data$Hazard)
hist(raw_data$Hazard, 200 , col = "blue")
summary(raw_data$Hazard)
quantile(raw_data$Hazard)
IQR(raw_data$Hazard)
boxplot(raw_data$Hazard)

#hazard_categories <- unique(raw_data$Hazard)
#table(hazard_categories)

#TODO: Improve this classification
#lets turn it into a binary classification problem, "rather crudely for now", based on the #3rd Quartile Value
# HazardScore <=5 as Low,  else as High # turn it into a function 
raw_data$Hazard_Categorical <- ifelse(raw_data$Hazard <=5 , "Low", "High")
table(raw_data$Hazard_Categorical)

#just in case we want to optimise this into a factor , since its categorical
#raw_data$Hazard_Categorical<- as.factor(raw_data$Hazard_Categorical)
#summary(raw_data$Hazard_Categorical)

#list of categorical columns "T1_V4","T1_V5","T1_V6","T1_V7","T1_V8","T1_V9","T1_V11","T1_V12","T1_V15","T1_V16","T1_V17","T2_V3","T2_V5","T2_V11","T2_V12","T2_V13"
#this below method not working if columns are wrapped in strings
#lets exclude all the non-categorical values
#remaining non-numerical fields at this stage Id,Hazard,Hazard_Categorical
non_categorical <- subset(raw_data, select=-c(T1_V4,T1_V5,T1_V6,T1_V7,T1_V8,T1_V9,T1_V11,T1_V12,T1_V15,T1_V16,T1_V17,T2_V3,T2_V5,T2_V11,T2_V12,T2_V13))
length(non_categorical)

# invoke normalisation method from clusterSim package, TODO: look if caret package has normalisation 
#remaining_categoricals <- subset(raw_data, select=-c(Id, Hazard,Hazard_Categorical))
#why are lapply and transform truncating my dataset and not mutating it in place ? TODO:see in dplyr ?
#normalise all columns except 1(Id),2(Hazard_Score) and 19(Hazard_Categorical)
#normalised <- data.Normalization (non_categorical[3:18],type="n1",normalization="column" )
#summary(normalised)
#colMeans(normalised)

normalised <- scale(non_categorical[3:18])
#normalised <- scale(non_categorical[3:18], scale=TRUE, center = FALSE)
#caret
#norm<-preProcess(non_categorical[3:18], method = c("center", "scale"))
#Attach truncated columns again #TODO: !this shouldnt be so 
#normalised$Id <- non_categorical$Id
#normalised$Hazard <- non_categorical$Hazard
normalised$Hazard_Categorical<- non_categorical$Hazard_Categorical
length(normalised$Hazard_Categorical)



#should have 17 at this point , 16 numerical predictors & 1 class label

#http://topepo.github.io/caret/training.html
#Partioning Data Based on the Hazard_Categorical variable 
#We might not need this as we have train and test in different csv's 
set.seed(998)
inTraining <- createDataPartition(normalised$Hazard_Categorical, p = .75, list = FALSE)
training <- normalised[ inTraining,]
testing  <- normalised[-inTraining,]
str(training)

#print correlation chart using PerformanceAnalytics package 
#taking too much time ? can we try this using Rapid Miner ? Scatter Matrix Charts ?
#chart.Correlation(non_categorical)

# an SVM classifier trained on an imbalanced dataset can produce suboptimal models which are biased
#towards the majority class and have low performance on the minority class
#tune the svm parameters
#One possible approach to test the performance of different gamma and cost combination values is
#to write a for loop to generate all the combinations of gamma and cost as inputs to train different support vector machines.
#Fortunately, SVM provides a tuning function, tune.svm, which makes the tuning much easier.
#?tune.svm
 
#?tune.svm
#tuned = tune.svm(x = training[-17],y = training$Hazard_Categorical, data = training,type="C", kernel = "linear", gamma = 2^(-1:1), cost = 2^(1:2))
#?svm from the e1701 package
#x predictor features , y = target variable, li
#model  = svm(Hazard_Categorical~., data = training, type="C", kernel="linear", gamma = tuned$best.parameters$gamma, cost = tuned$best.parameters$cost)
#summary(model)

#The shortcut Hazard_Categorical~. can be used to indicate that all of the columns in the data set (except y) should be used as a predictor.
model  = svm( Hazard_Categorical~., data = training,type="C", kernel="radial", cost=2, gamma = 1/ncol(training))
summary(model)

#model  = svm(x = training[-17],y = training$Hazard_Categorical, data = training,type="C", kernel="linear", cost=2, gamma = 1/ncol(training))
#summary(model)


#predict using the svm model generated above using the testing data frame, excluding the column whose Name matches Hazard_Categorical
pred = predict(model, testing[, !names(testing) %in% c("Hazard_Categorical")])

#
svm.table=table(pred, testing$Hazard_Categorical)
svm.table
classAgreement(svm.table)
confusionMatrix(svm.table)


#How do we plot svm in R ? 
#plot(x= normalised$T1_V1,y=normalised$T2_V1, col=normalised$Hazard_Categorical, pch=19)
#https://gist.github.com/multidis/8093372
#difference between scaling and normalisation?
#in.train
#summary(factor(non_categorical$Hazard))


?scale

require(stats)
x <- matrix(1:10, ncol = 2)
(centered.x <- scale(x, scale = FALSE))
cov(centered.scaled.x <- scale(x))
