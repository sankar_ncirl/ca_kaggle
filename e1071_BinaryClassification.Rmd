---
title: "SVM Binary Classification using e1071"
author: "Alva Lyons, Anthony McGrath, Vidya Sankar"
date: "February 28, 2016"
output: html_document
---

## Read Raw Data and Transform Hazard Score into Low High Categorical and create a data frame of numerical predictors with transformed hazard categorical predictor 
```{r}
raw_data = read.csv(file="train.csv", header = TRUE,sep = ",")
str(raw_data)

raw_data$Hazard_Categorical <- ifelse(raw_data$Hazard <=5 , "Low", "High")
table(raw_data$Hazard_Categorical)

non_categorical <- subset(raw_data, select=-c(T1_V4,T1_V5,T1_V6,T1_V7,T1_V8,T1_V9,T1_V11,T1_V12,T1_V15,T1_V16,T1_V17,T2_V3,T2_V5,T2_V11,T2_V12,T2_V13))
length(non_categorical)

normalised <- as.data.frame(scale(non_categorical[3:18]))
normalised$Hazard_Categorical<- non_categorical$Hazard_Categorical

```


##Transform classifier as boolean as needed for Tune  find best fit parameters for the model
```{r}
#The tune methods expects a boolean and not a categorical, create a separate df, transform a two level categorical to boolean
#with a predictor value as boolean
forTuning <- normalised
#false meaning Not Hazardous for low score and viceversa
forTuning$Hazard_Categorical <- ifelse(normalised$Hazard_Categorical == "Low", FALSE , TRUE)
forTuning

#split data at 75% , 25% and TODO : create a stratified sample ?? how do we restrict the count on this.
set.seed(998)
inTraining <- createDataPartition(normalised$Hazard_Categorical, p = .75, list = FALSE)
training <- normalised[ inTraining,]
testing  <- normalised[-inTraining,]
str(training)

```

##
```
#============ uncomment this code to run model without tuning =========================
#The shortcut Hazard_Categorical~. can be used to indicate that all of the columns in the data set (except y) should be used as a predictor.
# for SVM radial kernel
#model  = svm( Hazard_Categorical~., data = training,type="C", kernel="radial", cost=2, gamma = 1/ncol(training))
#summary(model)

# for SVM linear
#model  = svm( Hazard_Categorical~., data = training,type="C", kernel="linear", cost=2, gamma = 1/ncol(training))
#summary(model)

# ======== tuning the model - taking ages ! 
#https://www.safaribooksonline.com/library/view/machine-learning-with/9781783982042/ch06s06.html
#an SVM classifier trained on an imbalanced dataset can produce suboptimal models which are biased
#towards the majority class and have low performance on the minority class
#Hence tune the svm parameters
#One possible approach to test the performance of different gamma and cost combination values is
#to write a for loop to generate all the combinations of gamma and cost as inputs to train different SVM #Fortunately, SVM provides a tuning function, tune.svm, which makes the tuning much easier.

#?tune.svm
tuned = tune.svm(Hazard_Categorical~., data = forTuning, type="C", kernel = "linear", gamma = 2^(-1:1), cost = 2^(1:2),  tunecontrol = tune.control(sampling = "fix"))
summary(tuned)
```

##Generate model - based on cost and gamma parameters based on tuning
```{r}
model  = svm(Hazard_Categorical~., data = training, type="C", kernel="linear", gamma = tuned$best.parameters$gamma, cost = tuned$best.parameters$cost)
summary(model)

```
